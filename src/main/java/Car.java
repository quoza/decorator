public class Car implements ICar {
    private double chargerPressure;
    private double engineCapacity;
    private int seatsNumber;
    private boolean hasCharger;
    private double horsePower;

    public Car(double chargerPressure, double engineCapacity, int seatsNumber, boolean hasCharger, double horsePower) {
        this.chargerPressure = chargerPressure;
        this.engineCapacity = engineCapacity;
        this.seatsNumber = seatsNumber;
        this.hasCharger = hasCharger;
        this.horsePower = horsePower;
    }

    public double getHorsePower() {
        return horsePower;
    }

    public boolean hasCharger() {
        return hasCharger;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public double getChargerPressure() {
        return chargerPressure;
    }

    public void setChargerPressure(double chargerPressure) {
        this.chargerPressure = chargerPressure;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public boolean isHasCharger() {
        return hasCharger;
    }

    public void setHasCharger(boolean hasCharger) {
        this.hasCharger = hasCharger;
    }

    public void setHorsePower(double horsePower) {
        this.horsePower = horsePower;
    }
}

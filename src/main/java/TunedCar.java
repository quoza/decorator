public class TunedCar implements ICar {
    private ICar car;
    private boolean extraCharger;
    private boolean extraEngine;
    private boolean extraSeats;

    public TunedCar(ICar car, boolean extraCharger, boolean extraEngine) {
        this.car = car;
        if (car.hasCharger()){
            this.extraCharger = false;
        }
        this.extraEngine = extraEngine;
    }

    public double getHorsePower() {
        return car.getHorsePower() * (extraCharger ? 1.1 : 1.0) * (extraEngine ? 1.2 : 1.0);
    }

    public boolean hasCharger() {
        return car.hasCharger() || extraCharger;
    }

    public double getEngineCapacity() {
        return car.getEngineCapacity() + (extraEngine ? 0.3 : 0.0);
    }

    public double getChargerPressure() {
        return car.getChargerPressure() + (extraCharger ? 0.5 : 0.0);
    }
}
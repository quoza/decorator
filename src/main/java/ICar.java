public interface ICar {
    double getHorsePower();
    boolean hasCharger();
    double getEngineCapacity();
    double getChargerPressure();
}

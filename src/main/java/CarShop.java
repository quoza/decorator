public class CarShop {

    public ICar addExtraCharger(Car car){
        return new TunedCar(car, true, false);
    }

    public ICar addExtraEngine(Car car){
        return new TunedCar(car, false, true);
    }
}
